import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 constructor(private route : Router){}
  // userdata = [
  //   {
  //     DepartmentName: 'SuperAdmin',
  //     DepartmentCode:'adminLead1',
  //     password: '12345900',
  //     role: 'ROLE_SUPER_ADMIN'
  //   },
  //   {
  //     DepartmentName: 'IT',
  //     DepartmentCode:'IT101',
  //     password: '12345',
  //     role: 'ROLE_IT'
  //   },
  //   {
  //     DepartmentName: 'Bpo',
  //     password: '12345',
  //     DepartmentCode:'BPO101',
  //     role: 'ROLE_BPO'
  //   },
  //   {
  //     DepartmentName: 'production',
  //     password: '123435',
  //     DepartmentCode:'PRODUCTION101',
  //     role: 'ROLE_PRODUCTION'
  //   },
  //   {
  //     DepartmentName: 'college',
  //     password: '12345',
  //     DepartmentCode:'Collage101',
  //     role: 'ROLE_COLLEGE'
  //   }
  // ];

  public DepartmentName: string = '';
  public DepartmentCode:string='';
  public password: string = '';
  showPassword: boolean = false;
  loginForm: FormGroup;
 


  ngOnInit() { }

  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }

  // admin() {
  //   const user = this.userdata.find(user => user.DepartmentName === this.DepartmentName && user.password === this.password && this.DepartmentCode==this.DepartmentCode);
  //   if (user) {
  //     this.route.navigate(['admin-panel']);
  //     localStorage.setItem('userData',JSON.stringify(user));
  //   } else {
  //     alert('Invalid credentials');
  //   }
  // }
  // constructor(private fb: FormBuilder, private route:Router) {
  //   this.loginForm = this.fb.group({
  //     departmentname: ['', [Validators.required, Validators.minLength(3)]],
  //     departmentcode: ['', [Validators.required, Validators.minLength(2)]],
  //     password: ['', [Validators.required, Validators.minLength(6)]]
  //   });
  // }
  
  get departmentname() {
    return this.loginForm.get('departmentname');
  }

  get departmentcode() {
    return this.loginForm.get('departmentcode');
  }

  get passwordControl() {
    return this.loginForm.get('password');
  }

admin(){
  this.route.navigate(['admin-panel']);
}
signin(){
  this.route.navigate(['adminsignin'])

}
}
