import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AdminsigninComponent } from '../admin/adminsignin/adminsignin.component';

const routes: Routes = [
  {
    path:'header',component:HeaderComponent
},
{
    path:'',component:HomeComponent
},
{path:"login",component:LoginComponent},
{path:"adminsignin",component:AdminsigninComponent}
];

export const PagesRoutes = RouterModule.forChild(routes);
