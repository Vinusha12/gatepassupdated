import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { PagesRoutes } from './pages.routing';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutes,
    RouterModule,
    FormsModule,  
    ReactiveFormsModule  
  ],
  exports:[
    HeaderComponent
  ],
  declarations: [PagesComponent,HomeComponent,HeaderComponent,LoginComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class PagesModule { }
