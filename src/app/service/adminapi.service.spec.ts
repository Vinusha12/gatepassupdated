/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AdminapiService } from './adminapi.service';

describe('Service: Adminapi', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminapiService]
    });
  });

  it('should ...', inject([AdminapiService], (service: AdminapiService) => {
    expect(service).toBeTruthy();
  }));
});
