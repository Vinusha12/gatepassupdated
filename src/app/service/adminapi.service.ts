import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AdminapiService {

  private apiUrl = 'http://localhost:8080/admin';

constructor(private http : HttpClient) { }

adminRegister(userData: any): Observable<any> {
  return this.http.post(`${this.apiUrl}/register`, userData);
}








}
