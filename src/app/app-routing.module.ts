import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
{
  path:'pages',
  loadChildren:()=>import('../app/pages/pages.module').then(m=>m.PagesModule)
},

{path:"admin" , loadChildren:()=>import('../app/admin/admin.module').then(s=>s.AdminModule)},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
