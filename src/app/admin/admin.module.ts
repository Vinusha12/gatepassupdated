import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule } from '@angular/router';
import { PagesModule } from '../pages/pages.module';
import { AdminsigninComponent } from './adminsignin/adminsignin.component';

import { AdminRoutes } from './admin.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AdminRoutes,
    ReactiveFormsModule,
    FormsModule
 
  ],
  exports:[
    AdminHeaderComponent
  ],
  declarations: [AdminComponent,AdminsigninComponent,AdminHeaderComponent,AdminPanelComponent]
})
export class AdminModule { }
