import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminapiService } from 'src/app/service/adminapi.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

 userData:any;
  userForm: FormGroup;
  msg: string = "";

 
  ngOnInit(): void {
    
  }

  // ngOnInit() {
  //   this.createForm(); 
  //   let user = localStorage.getItem('userData');
  //   if(user){
  //     this.userData =JSON.parse(user);
  //     console.log(this.userData);
  //   }

  // }

//   createForm() {
//     this.userForm = this.formBuilder.group({
//       userName: ['', [Validators.required ,  Validators.maxLength(50),this.onlyLettersValidator()]],
//       password: ['', [Validators.required , Validators.minLength(6)]],
//       mobileNumber: ['', [Validators.required , Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^[0-9]+$')]],
//       department: ['', [Validators.required]],
//       email: ['', [Validators.required , Validators.email]],
//       role: ['', [Validators.required]],
//       uniqueId: ['', [Validators.required]]
//     });
//   }

//    // Custom validator function to allow only letters
// onlyLettersValidator(): any {
//   return (control: AbstractControl): { [key: string]: any } | null => {
//     const lettersRegex = /^[a-zA-Z]+$/;
//     if (!lettersRegex.test(control.value)) {
//       return { 'onlyLetters': true };
//     }
//     return null;
//   };
// }

  onSubmit() {
    
      this['adminService'].adminRegister(this.userForm.value).subscribe(
        (              response: any) => {
              console.log(response); // Handle the response from the service
            },
        (              error: any) => {
              console.error(error); // Handle any errors
            }
          );


}
[x: string]: any;
userName: any;
number: any;
moduledata: string | undefined;
emailForm: FormGroup | undefined;

showPassword = false;


post() {
throw new Error('Method not implemented.');
}
  signupForm: FormGroup;
  formBuilder: any;
register: any;
form: any;

  // });
  get passWord() {
    return this.signupForm.get('password');
  }
  get mobileNumber() {

    return this.signupForm.get('number');
  }

  get eMail() {
    return this.signupForm.get('email');
  }
  get role() {
    return this.signupForm.get('role');
  }



  constructor(private fb: FormBuilder,private route:Router) {
    this.signupForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      mobileNo: ['', [Validators.required, Validators.pattern('^[0-9]{10}$')]],
      department: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  
  
  gmailValidator(control: any) {
    if (control.value && !control.value.endsWith('@gmail.com')) {
      return { invalidEmail: true };
    }
    return null;
  }

}