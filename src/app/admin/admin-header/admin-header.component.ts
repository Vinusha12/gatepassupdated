import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {

  constructor(private router:Router) { }

  userData:any;
  
  ngOnInit() {
    let user = localStorage.getItem('userData');
    if(user){
      this.userData =JSON.parse(user);
      console.log(this.userData);
    }

  }
  logout(){
    localStorage.removeItem('userData');
    this.router.navigate([''])

  }
}
