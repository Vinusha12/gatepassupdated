import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-adminsignin',
  templateUrl: './adminsignin.component.html',
  styleUrls: ['./adminsignin.component.css']
})
export class AdminsigninComponent implements OnInit {

  [x: string]: any;
userName: any;
number: any;
moduledata: string | undefined;
emailForm: FormGroup | undefined;

showPassword = false;


post() {
throw new Error('Method not implemented.');
}
  signupForm: FormGroup;
  formBuilder: any;
register: any;
form: any;

  // });
  get passWord() {
    return this.signupForm.get('password');
  }
  get mobileNumber() {

    return this.signupForm.get('number');
  }

  get eMail() {
    return this.signupForm.get('email');
  }
  get role() {
    return this.signupForm.get('role');
  }

 
  ngOnInit() {
  }
  

  constructor(private fb: FormBuilder,private route:Router) {
    this.signupForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      mobileNo: ['', [Validators.required, Validators.pattern('^[0-9]{10}$')]],
      department: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  onSubmit() {
    if (this.signupForm.valid) {
      console.log('Form Submitted', this.signupForm.value);
    } else {
      console.log('Form not valid');
    }
  }
  
  gmailValidator(control: any) {
    if (control.value && !control.value.endsWith('@gmail.com')) {
      return { invalidEmail: true };
    }
    return null;
  }

adminpanel(){
  this.route.navigate(['adminpanel'])
}
}
