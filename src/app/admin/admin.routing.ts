import { Routes, RouterModule } from '@angular/router';
import { AdminsigninComponent } from './adminsignin/adminsignin.component';

import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';

const routes: Routes = [
  { path : 'adminpage' , component:AdminHeaderComponent},

 {path:'adminpanel',component:AdminPanelComponent},
];

export const AdminRoutes = RouterModule.forChild(routes);
